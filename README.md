# ApiRest 

Api Rest NodeJs Mysql

# Sinopsis

Es una Api el cuál está desarrollado con nodejs conectada a una base de datos

de MySq otorgandole credenciales para poder realizar las diferentes operaciones 

y asi poder interactuar el servidor con la base de datos y regresando información 

para que el cliente los pueda manipular de acuerdo a la petición.

El cliente está desarrollado con react node js y contiene estilos de css

y material-ui

# Requerimientos

NodeJs

express

mysql 

react

material-ui

react-dom

Se puedén instalar con npm install

# Uso 
Se utiliza para realizar la consulta de las peticiones de un cliente y el servidor

obtiene los datos de una DataBase llamado bajacalifornia(son datos obtenidos de la página de INEGI), y

de acuerdo a la petición retorna los resultados al cliente.

# INTEGRANTES

Avendaño Martinez Gustavo Israel

García Hernández Francisco de Jesus

Gerónimo Aparicio Elvia

Hernández Láscarez Humberto

Martínez León Daniel